# Things I want to test
# 1) Check that their names, age and hair colour are strings
# 2) Check their age is an integer

from strings import *

def test_first_name():
    assert type(first_name) == type("")

def test_last_name():
    assert type(last_name) == type("")

def test_eye_colour():
    assert type(eye_colour) == type("")

def test_hair_colour():
    assert type(hair_colour) == type("")

def test_my_age():
    assert type(age) == (int)
    