# Things I want to test
# 1) That there is a variable that is called "my_amazing_list"
# 2) Check it has 5 things inside
# 3) Check the type 

import pytest
from lists import *

# Check that my_amazing_list is a variable with length 5
def test_my_list():
    assert len(my_amazing_list) == 5

# Check that my_amazing_list is a type of 'list'
def test_my_amazing_list_is_list():
    assert type(my_amazing_list) == type([])
