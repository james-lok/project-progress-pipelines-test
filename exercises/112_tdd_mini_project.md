# Factory of Pão Alentejano (Portuguese Bread) Mini Project or Code along :bread:

Please read everything before you start. 

### Description of Mini project

This is a mini project where you'll individually plan and implement using Scrum, Agile and TDD. 

TDD - Test Driven Development - Is a simple way of implementing the Agile principles of "working code" as well a very iterative way of working.

Topics covered include:

- Functions
- Functions and programming best practices
  - DRY
  - Naming
  - Seperation of concerns
  - Testing
- TDD
- Documentation
- Agile
- Scrum

### The Task and Process

This task is to complete the user stories and follow all the acceptance criterea and Definition of Done (DoD). 

Please read everything before you start. 

Follow this process - THIS IS IMPORTANT TO FOLLOW TDD:

#### 1) Setup of Bo

1) Make a scrum board
2) Add user stories
3) **Add** acceptance criterea for EACH user story (these will become your testes)
4) Add the DoD for your project (applies to each card)
5) GET SHOW it to your TRAINER 

#### 2) SPRINT Planning

1) which task are most important? What is their business value
2) Which task are most dificult (planning poker)
3) What will you do in the next 1h? (first iteration)
4) What about the 1h after? (second iteration)

#### 3) Test Driven Development

1) Get one card
2) Use the acceptance criterea to make Testes
3) Build test for all the acceptence criterea of said card
4) Write the mininum amount of code for all the test to pass
5) Repeat steps 1-4 untill all card are done or sprint is over


### User Stories

#### User story 1

As a bread maker, I want to give to a function called `make_dough` some `water` and `plain flour`, and it will produce `dough`. Any other ingredients will produce `not dough`.

#### User story 2

As a bread maker, I want to give to a function called `bake_bread` some `dough`, and it will produce `bread`. Any other ingredients will produce `not bread`.

#### User story 3

As a bread maker, I want to give to a function called `run_factory` that will receive `water` and `plain flour`, call the necessary functions to make dough and bread. 

### Definition of Done - DoD

- Needs to be it's own project on git hub
- Needs documentation on how to run it AND how to run the tests
- Needs documentation link to Trello board
- All cards need at least 3 unit tests
- All tests should be passing
